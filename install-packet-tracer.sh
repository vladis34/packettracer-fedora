#!/bin/sh

echo 'on nettoie les anciennes installations de packet tracer'
sudo rm -rf /opt/pt
sudo rm -rf /usr/share/applications/cisco-pt7.desktop
sudo rm -rf /usr/share/applications/cisco-ptsa7.desktop
sudo rm -rf /usr/share/icons/hicolor/48x48/apps/pt7.png

echo 'On créé un répertoire temporaire pour extraire le contenu du paquet deb'
mkdir -p /tmp/pkt31

echo 'Copie du paquet PacketTracer_731_amd64.deb dans le répertoire /tmp/pkt31'
mv ~/Téléchargements/PacketTracer_731_amd64.deb /tmp/pkt31/PacketTracer_731_amd64.deb

echo 'on mets à jours les paquets'
sudo dnf update -y

echo 'on installe les prérequis'
sudo dnf install cmake make ark -y

echo 'on décompresse PacketTracer_731_amd64.deb avec ark'
/usr/bin/ark --batch --autodestination --autosubfolder /tmp/pkt31/PacketTracer_731_amd64.deb 2>/dev/null

echo 'on décompresse les dossiers control et data'
/usr/bin/ark --batch --autodestination --autosubfolder /tmp/pkt31/PacketTracer_731_amd64/control.tar.xz /tmp/pkt31/PacketTracer_731_amd64/data.tar.xz 2>/dev/null

echo 'on copie récursivement le contenu des dossiers usr et opt dans les répértoires racines /usr et /opt'
sudo cp -r /tmp/pkt31/PacketTracer_731_amd64/data/usr /
sudo cp -r /tmp/pkt31/PacketTracer_731_amd64/data/opt /

echo 'on créé le répértoire /root/.config'
sudo mkdir /root/.config

echo 'on configure l environnement de bureau'

sudo xdg-desktop-menu install /usr/share/applications/cisco-pt7.desktop
sudo xdg-desktop-menu install /usr/share/applications/cisco-ptsa7.desktop
sudo update-mime-database /usr/share/mime
sudo gtk-update-icon-cache --force --ignore-theme-index /usr/share/icons/gnome
sudo xdg-mime default cisco-ptsa7.desktop x-scheme-handler/pttp

echo 'on créé un lien de packet tracer dans /usr/local/bin/'
sudo ln -sf /opt/pt/packettracer /usr/local/bin/packettracer

echo 'on créé un répértoire pour installer la dépendance libjpeg'
mkdir /tmp/libjpeg

echo 'on télécharge le github qui contient la dépendance nécessaire à packettracer'
git clone https://github.com/libjpeg-turbo/libjpeg-turbo /tmp/libjpeg/

echo 'création du répértoire build pour la compilation'
mkdir /tmp/libjpeg/build

echo 'on se déplace dans le répértoire /tmp/libjpeg'
cd /tmp/libjpeg/

echo 'compilation de la dépendance dans le répértoire build'
sudo cmake -DWITH_JPEG8=1 -B /tmp/libjpeg/build/

echo 'on se déplace dans le répértoire de compilation'
cd /tmp/libjpeg/build

echo 'on lance la compliation de la dépendance'
sudo make

echo 'on copie le fichier libjpeg dans le répértoire /opt/pt/bin/'
sudo cp /tmp/libjpeg/build/libjpeg.so.8.2.2 /opt/pt/bin

echo 'on créé un lien symbolique du fichier /opt/pt/bin/libjpeg.so.8.2.2 vers /opt/pt/bin/libjpeg.so.8'
sudo ln -s /opt/pt/bin/libjpeg.so.8.2.2 /opt/pt/bin/libjpeg.so.8

echo 'on créé un lien symbolique de /usr/lib64/libdouble-conversion.so.3.1.5 vers /usr/lib64/libdouble-conversion.so.1'
sudo ln -sf /usr/lib64/libdouble-conversion.so.3.1.5 /usr/lib64/libdouble-conversion.so.1

echo 'installation réussi :) '

